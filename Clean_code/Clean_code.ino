//Download packages to be used later
#include "Adafruit_TCS34725.h" //ColorPAL Packages
#include <Servo.h> //Motor Package

 //Define RGB LED Pins
// use ~560  ohm resistor between Red & Blue, ~1K for green (its brighter)
#define redpin  45
#define greenpin 46
#define bluepin 44
//set to false if using a common cathode LED
#define commonAnode true

//ColorPal sensor and RGB value storage
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);
float red, green, blue;

//Define variables to track group scores
int totalIn = 0;
int count = 0;

//Declare motors
Servo servoLeft; 
Servo servoRight; 

//Declare QTI sensors
int qval1;
int qval2;
int qval3;
int qval4;

//Booleans to store if QTI senses Dark
boolean leftDark;
boolean middleDark;
boolean rightDark;
boolean QTI4;

//Variable to keep track of what segment of code to run
int mode = 0;

//Variable to store if mirror is found
boolean mirror = false;

//Variable to track time in second stage of code.
int twoCounter = 0;

//This array keeps track of what everyone else has sent - 0 = no 1 = waiting 2 = yes
int signals[]={1, 1, 1, 1, 1, 1};

//Function to receive QTI Values
long RCTime(int sensorIn){
  
  long duration = 0;
  pinMode(sensorIn, OUTPUT); // Sets pin as OUTPUT
  digitalWrite(sensorIn, HIGH); // Pin HIGH
  delay(1); // Waits for 1 millisecond
  
  pinMode(sensorIn, INPUT); // Sets pin as INPUT
  digitalWrite(sensorIn, LOW); // Pin LOW

  while(digitalRead(sensorIn)) { // Waits for the pin to go LOW
    duration++;
  }
  return duration; // Returns the duration of the pulse
}

//moves robot forward at specified speed.
void move(int speed){
  int lspeed = speed-90; // adjusts discrepancy

  //Moves motor and delays for time required
  servoLeft.writeMicroseconds(1500 + lspeed);
  servoRight.writeMicroseconds(1500 - speed);
}

//Clears LCD Screen
void clearS(){
  for (int i = 0; i<= 31; i++){
    Serial3.print(" ");
  }
}
 
void setup()                                 
{ 
  servoLeft.attach(11);                      // Attach left signal to pin 13
  servoRight.attach(12);                     // Attach right signal to pin 12

  //Initialize all 3 Serials, a debug, output, and LCD serial
  Serial.begin(9600);
  Serial2.begin(9600);   
  Serial3.begin(9600);

  //Clears LCD Screen
  clearS();

  //Initialize colorPAL
  if (tcs.begin()) {
    Serial.println("Found sensor");
  } 

  //Define LED Pins
  #if defined(ARDUINO_ARCH_ESP32)
    ledcAttachPin(redpin, 1);
    ledcSetup(1, 12000, 8);
    ledcAttachPin(greenpin, 2);
    ledcSetup(2, 12000, 8);
    ledcAttachPin(bluepin, 3);
    ledcSetup(3, 12000, 8);
  #else
    pinMode(redpin, OUTPUT);
    pinMode(greenpin, OUTPUT);
    pinMode(bluepin, OUTPUT);
  #endif
  pinMode(4, INPUT); // Button for transmission and debugging
}  


//Function to collect values for QTI Sensors
void getQTI(){
  qval1 = RCTime(49);
  qval2 = RCTime(51);
  qval3 = RCTime(53);
  qval4 = RCTime(47);
}

//Determines threshold bright and dark values
void determineDark(){
  if (qval1 > 80){
    leftDark = true;  
  } else {
    leftDark = false;
  }
  if (qval2 > 80){
    middleDark = true;  
  } else {
    middleDark = false;
  }
  if (qval3 > 80){
    rightDark = true;  
  } else {
    rightDark = false;
  }
  if (qval4<25){// Value of mirror is 25
    QTI4 = true;  
  } else {
    QTI4 = false;  
  }
}

void lineFollow(){
  if (middleDark && leftDark && rightDark){
    //Everything is dark.
    mode++;
    //Move onto search for mirror and reset motors
    servoRight.writeMicroseconds(1500);
    servoLeft.writeMicroseconds(1500);
  } else {
    if (middleDark && !leftDark && !rightDark){
      //Only middle sensor is dark, so we move forward
      move(200);
    } else if (leftDark && !rightDark){
      //Left line is Dark, right is not. this means we are crushing left line. Must turn right.
      servoRight.writeMicroseconds(1450);
      servoLeft.writeMicroseconds(1450);
    }
    else if (rightDark && !leftDark){
      //Right line is dark, left is not. Crushing right line. Must turn left.
      servoRight.writeMicroseconds(1550);
      servoLeft.writeMicroseconds(1550); 
    }
    if (!middleDark && !rightDark && !leftDark){
      //Everything is white, so we just keep moving forward
      move(200);
    }
  }
}

//Function to search for a mirror
void searchMirror(){
  //Begin an internal clock with twoCounter
  twoCounter++;
  
  if (twoCounter < 5){// Turn right for first 5 ticks.
    servoLeft.writeMicroseconds(1700);
    servoRight.writeMicroseconds(1700);
  }
  else if (twoCounter < 10){
    servoLeft.writeMicroseconds(1700);
    servoRight.writeMicroseconds(1300);
    //move Straight for next 5 ticks
    
    //If any qti sensor detects a value less than the mirror threshold, report positive for mirror
    if (QTI4){
      mirror = true; 
    }
    if (qval2 < 30){
      mirror = true;  
    }
    if (qval3 < 30){
      mirror = true;  
    }
    if (qval1 < 30){
      mirror = true;  
    }

    
  } else if (twoCounter < 30){
    //Stop and wait for another 20 ticks, all while continuing to scan for QTI
    servoLeft.writeMicroseconds(1500);
    servoRight.writeMicroseconds(1500);
    if (QTI4){
      mirror = true; 
    }
    if (qval2 < 30){
      mirror = true;  
    }
    if (qval3 < 30){
      mirror = true;  
    }
    if (qval1 < 30){
      mirror = true;  
    }
  } else if (twoCounter<43){
    //Continue to move straight to center bot over continent for colorPAL for another 13 ticks
    servoLeft.writeMicroseconds(1700);
    servoRight.writeMicroseconds(1300);
    
  } else {
    //Stop moving
    servoLeft.writeMicroseconds(1500);
    servoRight.writeMicroseconds(1500);
  }

  //If internal tick is at 44, determine whether to send an A or a, as well as increment our own
  //score. This insures we only send it once
  if (twoCounter == 44 && mirror == true){
    Serial2.print("A");
    signals[0] = 2;
    count++;
    totalIn++;
  }
  else if (twoCounter == 44 && mirror == false){
    signals[0] = 0;
    Serial2.print("a");
    totalIn++;
  }  
}


//function used to send signals - mainly used in earlier stages and for debugigng
void sendSignals(){
  if(digitalRead(4)){//If button is pressed
    char outgoing = 'A'; 
    Serial2.print(outgoing); // broadcast 'A' 
    
    if (outgoing == 'A'){ //Increment our own score - used to simulate a receiving signal
      count++;
    }
    totalIn++;
    //Simulate what our bot would do if we receieved an A.
  }
}


//function to receive XBee communciation signals.
void receiveSignals(){
  if (Serial2.available()){//Checks if signals are received
    char incoming = Serial2.read(); // reads incoming value

    //Checks if other bots sent high
    if (incoming == 'A'){
      count++;
      totalIn++;
      signals[0] = 2;  
    }
    if (incoming == 'B'){
      count++;
      totalIn++;
      signals[1] = 2;  
    }
    if (incoming == 'C'){
      count++;
      totalIn++;
      signals[2] = 2;  
    }
    if (incoming == 'D'){
      count++;
      totalIn++;
      signals[3] = 2;  
    }
    if (incoming == 'E'){
      count++;
      totalIn++;
      signals[4] = 2;  
    }
    if (incoming == 'F'){
      count++;
      totalIn++;
      signals[5] = 2;  
    }

    //Checks if other bots sent Low
    if (incoming == 'a'){
      totalIn++;
      signals[0] = 0;  
    }
    if (incoming == 'b'){
      totalIn++;
      signals[1] = 0;  
    }
    if (incoming == 'c'){
      totalIn++;
      signals[2] = 0;  
    }
    if (incoming == 'd'){
      totalIn++;
      signals[3] = 0;  
    }
    if (incoming == 'e'){
      totalIn++;
      signals[4] = 0;  
    }
    if (incoming == 'f'){
      totalIn++;
      signals[5] = 0;  
    }
  }
}

void determineCont(){
 if (abs(red - 95) < 10 && abs(green-102) < 10 && abs(blue-47)<10){ //Checks values of RGB to determine the right continent
    Serial.print("Green!");
    analogWrite(greenpin, 0);
    analogWrite(redpin, 255);
    analogWrite(bluepin, 255);
 }
 if (abs(red - 100) < 10 && abs(green-83) < 10 && abs(blue-60)<10){
    Serial.print("Gray!");
    analogWrite(redpin, 100);
    analogWrite(greenpin, 100);
    analogWrite(bluepin, 100);
 }
 if (abs(red - 115) < 10 && abs(green-88) < 10 && abs(blue-38)<10){
    Serial.print("Yellow!");
    analogWrite(redpin, 0);
    analogWrite(greenpin, 0);
    analogWrite(bluepin, 255);
 }
 if (abs(red - 93) < 10 && abs(green-78) < 10 && abs(blue-77)<10){
    Serial.print("Blue!");
    analogWrite(greenpin, 255);
    analogWrite(redpin, 255);
    analogWrite(bluepin, 0);
 }
 if (abs(red - 130) < 10 && abs(green-60) < 10 && abs(blue-60)<10){
    Serial.print("Purple!");
    analogWrite(greenpin, 255);
    analogWrite(redpin, 0);
    analogWrite(bluepin, 0);
 }
 if (red > 150 && abs(green-49) < 10 && abs(blue-40)<10){
    Serial.print("Red!");
    analogWrite(greenpin, 255);
    analogWrite(redpin, 0);
    analogWrite(bluepin, 255);
 }  
}

void printLCD(){
  Serial3.write(12);//Clear Screen
  Serial3.write(17);//Light up backboard
  Serial3.print("Val: ");
  Serial3.print(count);
  Serial3.print(" ");
  if(count==0) Serial3.print("Nitrogen");
  else if(count==1) Serial3.print("Health");
  else if(count==2) Serial3.print("Meds");
  else if(count==3) Serial3.print("VR");
  else if(count==4) Serial3.print("Tools");
  else if(count==5) Serial3.print("Brain");
  else if(count==6) Serial3.print("Learn");
  Serial3.write(13);//Next line
  Serial3.print("M: ");
  Serial3.print(mirror);
  Serial3.print(" Net: ");
  for (int i = 0; i < 6; i++){
    Serial3.print(signals[i]);
  }
  //Serial3.write(18);//Turns backlight off
}

void loop()                                  // Main loop auto-repeats
{  
  getQTI(); //Receive QTI Values
  determineDark(); //Convert QTI Values into booleans
  
  printLCD();//Prints LCD Values
  
  if (mode == 0){
    lineFollow();//Runs line follow, will increment mode when it is done
  }
  if (mode == 1){
    searchMirror();//Begins mirror search code
  }
  
  //Manually send high for debugging
  sendSignals();
  receiveSignals();

  //Gets RGB Values
  tcs.setInterrupt(false);  // turn on colorPAL LED
  delay(60);  // takes 60ms to read
  tcs.getRGB(&red, &green, &blue); //Update our variables with rgb values
  tcs.setInterrupt(true);  // turn off LED

  determineCont(); //Using RGB Values, determine continent we are on.

}
